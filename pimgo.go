package main

import (
	"gitlab.com/glasgowunion/pimgo/client/swagger"
)

// This is just an example of how you might start to use
// the golang API
// full client in client/swagger

/*

Request and Response Types are found
in go files named body_**.go

type Body40 struct {

	// Your PIM username
	Username string `json:"username"`

	// Your PIM password
	Password string `json:"password"`

	// Always equal to \"password\"
	GrantType string `json:"grant_type"`
}

*/

func main() {
	config := swagger.NewConfiguration()
	config.BasePath = "YOUR INSTALL URL"

	cli := swagger.NewAPIClient(config)

	body := swagger.Body40{
		Username:  "test",
		Password:  "",
		GrantType: "password"}

	locals := make(map[string]interface{})
	locals["body"] = body

	cli.AuthenticationApi("application/json", locals)

}
