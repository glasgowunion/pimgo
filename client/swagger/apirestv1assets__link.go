/* 
 * Akeneo PIM API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 1.0.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

// Links to get and download the variation file
type Apirestv1assetsLink struct {

	Self *Apirestv1assetsLinkSelf `json:"self,omitempty"`

	Download *Apirestv1assetsLinkDownload `json:"download,omitempty"`
}
