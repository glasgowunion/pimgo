# MeasureFamilyList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Measure family code | [default to null]
**Standard** | **string** | Measure family standard | [optional] [default to null]
**Units** | [**[]InlineResponse20013Units**](inline_response_200_13_units.md) | Family units | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


