# Apirestv1assetsReferenceFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Link** | [***Apirestv1assetsLink1**](apirestv1assets__link_1.md) |  | [optional] [default to null]
**Locale** | **string** | Locale code of the reference file | [optional] [default to null]
**Code** | **string** | Code of the reference file | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


