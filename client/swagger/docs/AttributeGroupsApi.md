# \AttributeGroupsApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AttributeGroupsGet**](AttributeGroupsApi.md#AttributeGroupsGet) | **Get** /api/rest/v1/attribute-groups/{code} | Get an attribute group
[**AttributeGroupsGetList**](AttributeGroupsApi.md#AttributeGroupsGetList) | **Get** /api/rest/v1/attribute-groups | Get list of attribute groups
[**AttributeGroupsPatch**](AttributeGroupsApi.md#AttributeGroupsPatch) | **Patch** /api/rest/v1/attribute-groups/{code} | Update/create an attribute group
[**AttributeGroupsPost**](AttributeGroupsApi.md#AttributeGroupsPost) | **Post** /api/rest/v1/attribute-groups | Create a new attribute group
[**SeveralAttributeGroupsPatch**](AttributeGroupsApi.md#SeveralAttributeGroupsPatch) | **Patch** /api/rest/v1/attribute-groups | Update/create several attribute groups


# **AttributeGroupsGet**
> InlineResponse2009 AttributeGroupsGet(code)
Get an attribute group

This endpoint allows you to get the information about a given attribute group.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse2009**](inline_response_200_9.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AttributeGroupsGetList**
> interface{} AttributeGroupsGetList(optional)
Get list of attribute groups

This endpoint allows you to get a list of attribute groups. Attribute groups are paginated and sorted by code.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AttributeGroupsPatch**
> AttributeGroupsPatch(code, body)
Update/create an attribute group

This endpoint allows you to update a given attribute group. Know more about <a href=\"/documentation/update.html#update-behavior\">Update behavior</a>. Note that if no attribute group exists for the given code, it creates it.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 
  **body** | [**Body23**](Body23.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AttributeGroupsPost**
> AttributeGroupsPost(optional)
Create a new attribute group

This endpoint allows you to create a new attribute group.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body21**](Body21.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SeveralAttributeGroupsPatch**
> InlineResponse200 SeveralAttributeGroupsPatch(optional)
Update/create several attribute groups

This endpoint allows you to update and/or create several attribute groups at once.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body22**](Body22.md)|  | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

