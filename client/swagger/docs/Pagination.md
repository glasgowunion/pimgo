# Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [***PaginationLinks**](Pagination__links.md) |  | [optional] [default to null]
**CurrentPage** | **int32** | Current page number | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


