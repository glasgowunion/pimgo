# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Product model code | [default to null]
**FamilyVariant** | **string** | Family variant code from which the product model inherits its attributes and variant attributes | [default to null]
**Parent** | **string** | Code of the parent product model | [optional] [default to null]
**Categories** | **[]string** | Codes of the categories in which the product model is categorized | [optional] [default to null]
**Values** | [***Apirestv1productmodelsValues**](apirestv1productmodels_values.md) |  | [optional] [default to null]
**Created** | **string** | Date of creation | [optional] [default to null]
**Updated** | **string** | Date of the last update | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


