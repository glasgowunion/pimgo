# \AuthenticationApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**PostToken**](AuthenticationApi.md#PostToken) | **Post** /api/oauth/v1/token | Get authentication token


# **PostToken**
> InlineResponse20022 PostToken(contentType, authorization, optional)
Get authentication token

This endpoint allows you to get an authentication token. No need to be authenticated to use this endpoint.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **contentType** | **string**| Equal to &#39;application/json&#39; or &#39;application/x-www-form-urlencoded&#39;, no other value allowed | 
  **authorization** | **string**| Equal to &#39;Basic xx&#39;, where &#39;xx&#39; is the base 64 encoding of the client id and secret. Find out how to generate them in the &lt;a href&#x3D;\&quot;/documentation/security.html#create-an-oauth-client\&quot;&gt;Create an OAuth client&lt;/a&gt; section. | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **string**| Equal to &#39;application/json&#39; or &#39;application/x-www-form-urlencoded&#39;, no other value allowed | 
 **authorization** | **string**| Equal to &#39;Basic xx&#39;, where &#39;xx&#39; is the base 64 encoding of the client id and secret. Find out how to generate them in the &lt;a href&#x3D;\&quot;/documentation/security.html#create-an-oauth-client\&quot;&gt;Create an OAuth client&lt;/a&gt; section. | 
 **body** | [**Body40**](Body40.md)|  | 

### Return type

[**InlineResponse20022**](inline_response_200_22.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

