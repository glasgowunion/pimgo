# InlineResponse20017

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Code of the asset reference file | [optional] [default to null]
**Locale** | **string** | Locale of the asset reference file, equal to &#x60;null&#x60; if the asset is not localizable | [optional] [default to null]
**Link** | [***InlineResponse20017Link**](inline_response_200_17__link.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


