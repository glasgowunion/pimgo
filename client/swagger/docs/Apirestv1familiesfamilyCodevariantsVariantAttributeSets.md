# Apirestv1familiesfamilyCodevariantsVariantAttributeSets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Level** | **int32** | Enrichment level | [default to null]
**Axes** | **[]string** | Codes of attributes used as variant axes | [default to null]
**Attributes** | **[]string** | Codes of attributes bind to this enrichment level | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


