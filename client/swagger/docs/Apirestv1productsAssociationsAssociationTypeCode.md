# Apirestv1productsAssociationsAssociationTypeCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Groups** | **[]string** | Array of groups codes with which the product is in relation | [optional] [default to null]
**Products** | **[]string** | Array of product identifiers with which the product is in relation | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


