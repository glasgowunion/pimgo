# Body31

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Asset code | [default to null]
**Categories** | **[]string** | Codes of the asset categories in which the asset is classified | [optional] [default to null]
**Description** | **string** | Description of the asset | [optional] [default to null]
**Localizable** | **bool** | Whether the asset is localized or not, meaning if you want to have different reference files for each of your locale | [optional] [default to null]
**Tags** | **[]string** | Tags of the asset | [optional] [default to null]
**EndOfUse** | **string** | Date on which the asset expire | [optional] [default to null]
**VariationFiles** | [**[]Apirestv1assetsVariationFiles**](apirestv1assets_variation_files.md) | Variations of the asset | [optional] [default to null]
**ReferenceFiles** | [**[]Apirestv1assetsReferenceFiles**](apirestv1assets_reference_files.md) | Reference files of the asset | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


