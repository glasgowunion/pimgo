# \AttributeOptionsApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAttributesAttributeCodeOptions**](AttributeOptionsApi.md#GetAttributesAttributeCodeOptions) | **Get** /api/rest/v1/attributes/{attribute_code}/options | Get list of attribute options
[**GetAttributesAttributeCodeOptionsCode**](AttributeOptionsApi.md#GetAttributesAttributeCodeOptionsCode) | **Get** /api/rest/v1/attributes/{attribute_code}/options/{code} | Get an attribute option
[**PatchAttributesAttributeCodeOptions**](AttributeOptionsApi.md#PatchAttributesAttributeCodeOptions) | **Patch** /api/rest/v1/attributes/{attribute_code}/options | Update/create several attribute options
[**PatchAttributesAttributeCodeOptionsCode**](AttributeOptionsApi.md#PatchAttributesAttributeCodeOptionsCode) | **Patch** /api/rest/v1/attributes/{attribute_code}/options/{code} | Update/create an attribute option
[**PostAttributesAttributeCodeOptions**](AttributeOptionsApi.md#PostAttributesAttributeCodeOptions) | **Post** /api/rest/v1/attributes/{attribute_code}/options | Create a new attribute option


# **GetAttributesAttributeCodeOptions**
> interface{} GetAttributesAttributeCodeOptions(attributeCode, optional)
Get list of attribute options

This endpoint allows you to get a list of attribute options. Attribute options are paginated and sorted by code.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **attributeCode** | **string**| Code of the attribute | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attributeCode** | **string**| Code of the attribute | 
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAttributesAttributeCodeOptionsCode**
> InlineResponse2008 GetAttributesAttributeCodeOptionsCode(attributeCode, code)
Get an attribute option

This endpoint allows you to get the information about a given attribute option.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **attributeCode** | **string**| Code of the attribute | 
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse2008**](inline_response_200_8.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchAttributesAttributeCodeOptions**
> InlineResponse200 PatchAttributesAttributeCodeOptions(attributeCode, optional)
Update/create several attribute options

This endpoint allows you to update several attribute options at once.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **attributeCode** | **string**| Code of the attribute | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attributeCode** | **string**| Code of the attribute | 
 **body** | [**Body19**](Body19.md)|  | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchAttributesAttributeCodeOptionsCode**
> PatchAttributesAttributeCodeOptionsCode(attributeCode, code, body)
Update/create an attribute option

This endpoint allows you to update a given attribute option. Know more about <a href=\"/documentation/update.html#update-behavior\">Update behavior</a>. Note that if no attribute option exists for the given code, it creates it.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **attributeCode** | **string**| Code of the attribute | 
  **code** | **string**| Code of the resource | 
  **body** | [**Body20**](Body20.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostAttributesAttributeCodeOptions**
> PostAttributesAttributeCodeOptions(attributeCode, optional)
Create a new attribute option

This endpoint allows you to create a new attribute option.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **attributeCode** | **string**| Code of the attribute | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attributeCode** | **string**| Code of the attribute | 
 **body** | [**Body18**](Body18.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

