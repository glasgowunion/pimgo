# MediaFileList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Media file code | [optional] [default to null]
**OriginalFilename** | **string** | Original filename of the media file | [optional] [default to null]
**MimeType** | **string** | Mime type of the media file | [optional] [default to null]
**Size** | **int32** | Size of the media file | [optional] [default to null]
**Extension** | **string** | Extension of the media file | [optional] [default to null]
**Links** | [***InlineResponse20015Links**](inline_response_200_15__links.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


