# InlineResponse20021

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Host** | **string** | Host name | [optional] [default to null]
**Authentication** | [***interface{}**](interface{}.md) | Endpoint to get the authentication token | [optional] [default to null]
**Routes** | [***interface{}**](interface{}.md) | All the availables endpoints | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


