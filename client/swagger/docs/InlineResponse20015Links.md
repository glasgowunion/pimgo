# InlineResponse20015Links

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Download** | [***InlineResponse20015LinksDownload**](inline_response_200_15__links_download.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


