# Body34

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**File** | **string** | The binaries of the file | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


