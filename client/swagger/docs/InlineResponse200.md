# InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Line** | **int32** | Line number | [optional] [default to null]
**Identifier** | **string** | Resource identifier, only filled when the resource is a product | [optional] [default to null]
**Code** | **string** | Resource code, only filled when the resource is a category, a family or an attribute | [optional] [default to null]
**StatusCode** | **int32** | HTTP status code, see &lt;a href&#x3D;\&quot;/documentation/responses.html#client-errors\&quot;&gt;Client errors&lt;/a&gt; to understand the meaning of each code | [optional] [default to null]
**Message** | **string** | Message explaining the error | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


