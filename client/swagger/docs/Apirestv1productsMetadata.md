# Apirestv1productsMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WorkflowStatus** | **string** | Status of the product regarding the user permissions (only available in the v2.x Enterprise Edition) | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


