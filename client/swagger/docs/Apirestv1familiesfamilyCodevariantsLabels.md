# Apirestv1familiesfamilyCodevariantsLabels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LocaleCode** | **string** | Family variant label for the locale &#x60;localeCode&#x60; | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


