# MediaFileItemListLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [***MediaFileItemListLinksSelf**](MediaFileItemList__links_self.md) |  | [optional] [default to null]
**Download** | [***MediaFileItemListLinksDownload**](MediaFileItemList__links_download.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


