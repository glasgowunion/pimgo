# InlineResponse20018Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Download** | [***Apirestv1assetsLinkDownload**](apirestv1assets__link_download.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


