# Body2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Identifier** | **string** | Product identifier, i.e. the value of the only &#x60;pim_catalog_identifier&#x60; attribute | [default to null]
**Enabled** | **bool** | Whether the product is enable | [optional] [default to null]
**Family** | **string** | Family code from which the product inherits its attributes and attributes requirements | [optional] [default to null]
**Categories** | **[]string** | Codes of the categories in which the product is classified | [optional] [default to null]
**Groups** | **[]string** | Codes of the groups to which the product belong | [optional] [default to null]
**Parent** | **string** | Code of the parent product model when the product is a variant (only available in 2.x) | [optional] [default to null]
**Values** | [***Apirestv1productsValues**](apirestv1products_values.md) |  | [optional] [default to null]
**Associations** | [***Apirestv1productsAssociations**](apirestv1products_associations.md) |  | [optional] [default to null]
**Created** | **string** | Date of creation | [optional] [default to null]
**Updated** | **string** | Date of the last update | [optional] [default to null]
**Metadata** | [***Apirestv1productsMetadata**](apirestv1products_metadata.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


