# Apirestv1assetsVariationFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Link** | [***Apirestv1assetsLink**](apirestv1assets__link.md) |  | [optional] [default to null]
**Locale** | **string** | Locale code of the variation | [optional] [default to null]
**Scope** | **string** | Channel code of the variation | [optional] [default to null]
**Code** | **string** | Code of the variation | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


