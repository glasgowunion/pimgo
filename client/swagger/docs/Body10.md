# Body10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Family code | [default to null]
**AttributeAsLabel** | **string** | Attribute code used as label | [default to null]
**AttributeAsImage** | **string** | Attribute code used as the main picture in the user interface (only since v2.0) | [optional] [default to null]
**Attributes** | **[]string** | Attributes codes that compose the family | [optional] [default to null]
**AttributeRequirements** | [***Apirestv1familiesAttributeRequirements**](apirestv1families_attribute_requirements.md) |  | [optional] [default to null]
**Labels** | [***Apirestv1familiesLabels**](apirestv1families_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


