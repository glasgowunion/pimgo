# AssetVariationFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Code of the asset variation file | [optional] [default to null]
**Locale** | **string** | Locale of the asset variation file, equal to &#x60;null&#x60; if the asset is not localizable | [optional] [default to null]
**Scope** | **string** | Channel of the asset variation file | [optional] [default to null]
**Link** | [***InlineResponse20018Link**](inline_response_200_18__link.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


