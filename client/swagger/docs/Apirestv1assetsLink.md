# Apirestv1assetsLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [***Apirestv1assetsLinkSelf**](apirestv1assets__link_self.md) |  | [optional] [default to null]
**Download** | [***Apirestv1assetsLinkDownload**](apirestv1assets__link_download.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


