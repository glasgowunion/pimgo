# InlineResponse2003Values

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AttributeCode** | [**[]Apirestv1productsValuesAttributeCode**](apirestv1products_values_attributeCode.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


