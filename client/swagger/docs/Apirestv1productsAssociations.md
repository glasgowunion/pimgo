# Apirestv1productsAssociations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssociationTypeCode** | [***Apirestv1productsAssociationsAssociationTypeCode**](apirestv1products_associations_associationTypeCode.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


