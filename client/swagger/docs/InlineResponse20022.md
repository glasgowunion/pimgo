# InlineResponse20022

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AccessToken** | **string** | Authentication token that should be given in every authenticated request to the API | [optional] [default to null]
**ExpiresIn** | **int32** | Validity of the token given in seconds, 3600s &#x3D; 1h by default | [optional] [default to null]
**TokenType** | **string** | Token type, always equal to \&quot;bearer\&quot; | [optional] [default to null]
**Scope** | **string** | Unused, always equal to \&quot;null\&quot; | [optional] [default to null]
**RefreshToken** | **string** | Use this token when your access token has expired. See &lt;a href&#x3D;\&quot;/documentation/security.html#refresh-an-expired-token\&quot;&gt;Refresh an expired token&lt;/a&gt; section for more details. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


