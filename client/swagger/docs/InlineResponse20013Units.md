# InlineResponse20013Units

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Measure code | [optional] [default to null]
**Convert** | [***interface{}**](interface{}.md) | Mathematic operation to convert the unit into the standard unit | [optional] [default to null]
**Symbol** | **string** | Measure symbol | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


