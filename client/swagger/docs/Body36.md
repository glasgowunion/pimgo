# Body36

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Asset category code | [default to null]
**Parent** | **string** | Asset category code of the parent&#39;s asset category | [optional] [default to null]
**Labels** | [***Apirestv1assetcategoriesLabels**](apirestv1assetcategories_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


