# PaginationLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [***PaginationLinksSelf**](Pagination__links_self.md) |  | [optional] [default to null]
**First** | [***PaginationLinksFirst**](Pagination__links_first.md) |  | [optional] [default to null]
**Previous** | [***PaginationLinksPrevious**](Pagination__links_previous.md) |  | [optional] [default to null]
**Next** | [***PaginationLinksNext**](Pagination__links_next.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


