# \MediaFilesApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetMediaFiles**](MediaFilesApi.md#GetMediaFiles) | **Get** /api/rest/v1/media-files | Get a list of media files
[**GetMediaFilesCode**](MediaFilesApi.md#GetMediaFilesCode) | **Get** /api/rest/v1/media-files/{code} | Get a media file
[**GetMediaFilesCodeDownload**](MediaFilesApi.md#GetMediaFilesCodeDownload) | **Get** /api/rest/v1/media-files/{code}/download | Download a media file
[**PostMediaFiles**](MediaFilesApi.md#PostMediaFiles) | **Post** /api/rest/v1/media-files | Create a new media file


# **GetMediaFiles**
> interface{} GetMediaFiles(optional)
Get a list of media files

This endpoint allows you to get a list of media files.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetMediaFilesCode**
> InlineResponse20015 GetMediaFilesCode(code)
Get a media file

This endpoint allows you to get the information about a given media file.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse20015**](inline_response_200_15.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetMediaFilesCodeDownload**
> GetMediaFilesCodeDownload(code)
Download a media file

This endpoint allows you to download a given media file.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostMediaFiles**
> PostMediaFiles(contentType, optional)
Create a new media file

This endpoint allows you to create a new media file and associate it to an attribute value of a given product or product model.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **contentType** | **string**| Equal to &#39;multipart/form-data&#39;, no other value allowed | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **string**| Equal to &#39;multipart/form-data&#39;, no other value allowed | 
 **body** | [**Body30**](Body30.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

