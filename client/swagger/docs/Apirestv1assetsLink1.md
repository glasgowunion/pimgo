# Apirestv1assetsLink1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [***Apirestv1assetsLink1Self**](apirestv1assets__link_1_self.md) |  | [optional] [default to null]
**Download** | [***Apirestv1assetsLink1Download**](apirestv1assets__link_1_download.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


