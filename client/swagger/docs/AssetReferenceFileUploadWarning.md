# AssetReferenceFileUploadWarning

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | **string** | Message explaining the warning | [optional] [default to null]
**Errors** | [**[]InlineResponse201Errors**](inline_response_201_errors.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


