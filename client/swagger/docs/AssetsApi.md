# \AssetsApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAssets**](AssetsApi.md#GetAssets) | **Get** /api/rest/v1/assets | Get list of assets
[**GetAssetsCode**](AssetsApi.md#GetAssetsCode) | **Get** /api/rest/v1/assets/{code} | Get an asset
[**GetReferenceFilesChannelCodeLocaleCodeDownload**](AssetsApi.md#GetReferenceFilesChannelCodeLocaleCodeDownload) | **Get** /api/rest/v1/assets/{asset_code}/reference-files/{locale_code}/download | Download a reference file
[**GetReferenceFilesLocaleCode**](AssetsApi.md#GetReferenceFilesLocaleCode) | **Get** /api/rest/v1/assets/{asset_code}/reference-files/{locale_code} | Get a reference file
[**GetVariationFilesChannelCodeLocaleCode**](AssetsApi.md#GetVariationFilesChannelCodeLocaleCode) | **Get** /api/rest/v1/assets/{asset_code}/variation-files/{channel_code}/{locale_code} | Get a variation file
[**GetVariationFilesChannelCodeLocaleCodeDownload**](AssetsApi.md#GetVariationFilesChannelCodeLocaleCodeDownload) | **Get** /api/rest/v1/assets/{asset_code}/variation-files/{channel_code}/{locale_code}/download | Download a variation file
[**PatchAssets**](AssetsApi.md#PatchAssets) | **Patch** /api/rest/v1/assets | Update/create several assets
[**PatchAssetsCode**](AssetsApi.md#PatchAssetsCode) | **Patch** /api/rest/v1/assets/{code} | Update/create an asset
[**PostAssets**](AssetsApi.md#PostAssets) | **Post** /api/rest/v1/assets | Create a new asset
[**PostReferenceFilesLocaleCode**](AssetsApi.md#PostReferenceFilesLocaleCode) | **Post** /api/rest/v1/assets/{asset_code}/reference-files/{locale_code} | Upload a new reference file
[**PostVariationFilesChannelCodeLocaleCode**](AssetsApi.md#PostVariationFilesChannelCodeLocaleCode) | **Post** /api/rest/v1/assets/{asset_code}/variation-files/{channel_code}/{locale_code} | Upload a new variation file


# **GetAssets**
> interface{} GetAssets(optional)
Get list of assets

This endpoint allows you to get a list of assets. Assets are paginated.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **paginationType** | **string**| Pagination method type, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to page]
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **searchAfter** | **string**| Cursor when using the &#x60;search_after&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to cursor to the first page]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAssetsCode**
> InlineResponse20016 GetAssetsCode(code)
Get an asset

This endpoint allows you to get the information about a given asset.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse20016**](inline_response_200_16.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetReferenceFilesChannelCodeLocaleCodeDownload**
> GetReferenceFilesChannelCodeLocaleCodeDownload(assetCode, localeCode)
Download a reference file

This endpoint allows you to download a given reference file.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **assetCode** | **string**| Code of the asset | 
  **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetReferenceFilesLocaleCode**
> InlineResponse20017 GetReferenceFilesLocaleCode(assetCode, localeCode)
Get a reference file

This endpoint allows you to get the information about a reference file of a given asset.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **assetCode** | **string**| Code of the asset | 
  **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 

### Return type

[**InlineResponse20017**](inline_response_200_17.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVariationFilesChannelCodeLocaleCode**
> InlineResponse20018 GetVariationFilesChannelCodeLocaleCode(assetCode, channelCode, localeCode)
Get a variation file

This endpoint allows you to get the information about a variation file of a given asset.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **assetCode** | **string**| Code of the asset | 
  **channelCode** | **string**| Code of the channel | 
  **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 

### Return type

[**InlineResponse20018**](inline_response_200_18.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetVariationFilesChannelCodeLocaleCodeDownload**
> GetVariationFilesChannelCodeLocaleCodeDownload(assetCode, channelCode, localeCode)
Download a variation file

This endpoint allows you to download a given variation file.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **assetCode** | **string**| Code of the asset | 
  **channelCode** | **string**| Code of the channel | 
  **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchAssets**
> InlineResponse200 PatchAssets(optional)
Update/create several assets

This endpoint allows you to update several assets at once.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body32**](Body32.md)|  | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchAssetsCode**
> PatchAssetsCode(code, body)
Update/create an asset

This endpoint allows you to update a given asset. Know more about <a href=\"/documentation/update.html#update-behavior\">Update behavior</a>. Note that if no asset exists for the given code, it creates it.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 
  **body** | [**Body33**](Body33.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostAssets**
> PostAssets(optional)
Create a new asset

This endpoint allows you to create a new asset.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body31**](Body31.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostReferenceFilesLocaleCode**
> InlineResponse201 PostReferenceFilesLocaleCode(assetCode, localeCode, contentType, optional)
Upload a new reference file

This endpoint allows you to upload a new reference file for a given asset and locale. It will also automatically generate all the variation files corresponding to this reference file.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **assetCode** | **string**| Code of the asset | 
  **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 
  **contentType** | **string**| Equal to &#39;multipart/form-data&#39;, no other value allowed | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetCode** | **string**| Code of the asset | 
 **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 
 **contentType** | **string**| Equal to &#39;multipart/form-data&#39;, no other value allowed | 
 **body** | [**Body34**](Body34.md)|  | 

### Return type

[**InlineResponse201**](inline_response_201.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostVariationFilesChannelCodeLocaleCode**
> PostVariationFilesChannelCodeLocaleCode(assetCode, channelCode, localeCode, contentType, optional)
Upload a new variation file

This endpoint allows you to upload a new variation file for a given asset, channel and locale.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **assetCode** | **string**| Code of the asset | 
  **channelCode** | **string**| Code of the channel | 
  **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 
  **contentType** | **string**| Equal to &#39;multipart/form-data&#39;, no other value allowed | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetCode** | **string**| Code of the asset | 
 **channelCode** | **string**| Code of the channel | 
 **localeCode** | **string**| Code of the locale if the asset is localizable or equal to &#x60;no-locale&#x60; if the asset is not localizable | 
 **contentType** | **string**| Equal to &#39;multipart/form-data&#39;, no other value allowed | 
 **body** | [**Body35**](Body35.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

