# Apirestv1productsValuesAttributeCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Scope** | **string** | Channel code of the product value | [optional] [default to null]
**Locale** | **string** | Locale code of the product value | [optional] [default to null]
**Data** | [***interface{}**](interface{}.md) | Product value | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


