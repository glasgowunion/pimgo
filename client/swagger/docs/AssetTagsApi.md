# \AssetTagsApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAssetTags**](AssetTagsApi.md#GetAssetTags) | **Get** /api/rest/v1/asset-tags | Get list of asset tags
[**GetAssetTagsCode**](AssetTagsApi.md#GetAssetTagsCode) | **Get** /api/rest/v1/asset-tags/{code} | Get an asset tag
[**PatchAssetTagsCode**](AssetTagsApi.md#PatchAssetTagsCode) | **Patch** /api/rest/v1/asset-tags/{code} | Update/create an asset tag


# **GetAssetTags**
> interface{} GetAssetTags(optional)
Get list of asset tags

This endpoint allows you to get a list of asset tags. Asset tags are paginated.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetAssetTagsCode**
> InlineResponse20020 GetAssetTagsCode(code)
Get an asset tag

This endpoint allows you to get the information about a given asset tag.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse20020**](inline_response_200_20.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchAssetTagsCode**
> PatchAssetTagsCode(code, body)
Update/create an asset tag

This endpoint allows you to update a given asset tag. Know more about <a href=\"/documentation/update.html#update-behavior\">Update behavior</a>. Note that if no tag exists for the given code, it creates it.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 
  **body** | [**Body39**](Body39.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

