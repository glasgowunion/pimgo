# Apirestv1assetsLinkSelf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Href** | **string** | URI of the variation entity | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


