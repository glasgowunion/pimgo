# AttributeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Attribute code | [default to null]
**Type_** | **string** | Attribute type | [default to null]
**Labels** | [***Apirestv1attributesLabels**](apirestv1attributes_labels.md) |  | [optional] [default to null]
**Group** | **string** | Attribute group | [default to null]
**SortOrder** | **int32** | Order of the attribute in its group | [optional] [default to null]
**Localizable** | **bool** | Whether the attribute is localizable, i.e. can have one value by locale | [optional] [default to null]
**Scopable** | **bool** | Whether the attribute is scopable, i.e. can have one value by channel | [optional] [default to null]
**AvailableLocales** | **[]string** | To make the attribute locale specfic, specify here for which locales it is specific | [optional] [default to null]
**Unique** | **bool** | Whether two values for the attribute cannot be the same | [optional] [default to null]
**UseableAsGridFilter** | **bool** | Whether the attribute can be used as a filter for the product grid in the PIM user interface | [optional] [default to null]
**MaxCharacters** | **int32** | Number maximum of characters allowed for the value of the attribute when the attribute type is &#x60;pim_catalog_text&#x60;, &#x60;pim_catalog_textarea&#x60; or &#x60;pim_catalog_identifier&#x60; | [optional] [default to null]
**ValidationRule** | **string** | Validation rule type used to validate any attribute value when the attribute type is &#x60;pim_catalog_text&#x60; or &#x60;pim_catalog_identifier&#x60; | [optional] [default to null]
**ValidationRegexp** | **string** | Regexp expression used to validate any attribute value when the attribute type is &#x60;pim_catalog_text&#x60; or &#x60;pim_catalog_identifier&#x60; | [optional] [default to null]
**WysiwygEnabled** | **bool** | Whether the WYSIWYG interface is shown when the attribute type is &#x60;pim_catalog_textarea&#x60; | [optional] [default to null]
**NumberMin** | **string** | Minimum integer value allowed when the attribute type is &#x60;pim_catalog_metric&#x60;, &#x60;pim_catalog_price&#x60; or &#x60;pim_catalog_number&#x60; | [optional] [default to null]
**NumberMax** | **string** | Minimum integer value allowed when the attribute type is &#x60;pim_catalog_metric&#x60;, &#x60;pim_catalog_price&#x60; or &#x60;pim_catalog_number&#x60; | [optional] [default to null]
**DecimalsAllowed** | **bool** | Whether decimals are allowed when the attribute type is &#x60;pim_catalog_metric&#x60;, &#x60;pim_catalog_price&#x60; or &#x60;pim_catalog_number&#x60; | [optional] [default to null]
**NegativeAllowed** | **bool** | Whether negative values are allowed when the attribute type is &#x60;pim_catalog_metric&#x60; or &#x60;pim_catalog_number&#x60; | [optional] [default to null]
**MetricFamily** | **string** | Metric family when the attribute type is &#x60;pim_catalog_metric&#x60; | [optional] [default to null]
**DefaultMetricUnit** | **string** | Default metric unit when the attribute type is &#x60;pim_catalog_metric&#x60; | [optional] [default to null]
**DateMin** | [**time.Time**](time.Time.md) | Minimum date allowed when the attribute type is &#x60;pim_catalog_date&#x60; | [optional] [default to null]
**DateMax** | [**time.Time**](time.Time.md) | Maximum date allowed when the attribute type is &#x60;pim_catalog_date&#x60; | [optional] [default to null]
**AllowedExtensions** | **[]string** | Extensions allowed when the attribute type is &#x60;pim_catalog_file&#x60; or &#x60;pim_catalog_image&#x60; | [optional] [default to null]
**MaxFileSize** | **string** | Max file size when the attribute type is &#x60;pim_catalog_file&#x60; or &#x60;pim_catalog_image&#x60; | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


