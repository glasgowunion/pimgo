# InlineResponse20017Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Download** | [***Apirestv1assetsLink1Download**](apirestv1assets__link_1_download.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


