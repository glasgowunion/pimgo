# Body13

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Family variant code | [default to null]
**VariantAttributeSets** | [**[]Apirestv1familiesfamilyCodevariantsVariantAttributeSets**](apirestv1familiesfamily_codevariants_variant_attribute_sets.md) | Attributes distribution according to the enrichment level | [default to null]
**Labels** | [***Apirestv1familiesfamilyCodevariantsLabels**](apirestv1familiesfamily_codevariants_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


