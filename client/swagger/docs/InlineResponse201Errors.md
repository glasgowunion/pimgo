# InlineResponse201Errors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Channel** | **string** | Channel for which the variation file generation failed | [optional] [default to null]
**Locale** | **string** | Locale for which the variation file generation failed | [optional] [default to null]
**Message** | **string** | Message explaining why the variation file generation failed | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


