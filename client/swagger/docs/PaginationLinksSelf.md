# PaginationLinksSelf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Href** | **string** | URI of the current page of resources | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


