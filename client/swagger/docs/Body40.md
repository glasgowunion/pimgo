# Body40

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | Your PIM username | [default to null]
**Password** | **string** | Your PIM password | [default to null]
**GrantType** | **string** | Always equal to \&quot;password\&quot; | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


