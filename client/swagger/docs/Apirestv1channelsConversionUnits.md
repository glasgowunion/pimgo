# Apirestv1channelsConversionUnits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AttributeCode** | **string** | Conversion unit code used to convert the values of the attribute &#x60;attributeCode&#x60; when exporting via the channel | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


