# Body22

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Attribute group code | [default to null]
**SortOrder** | **int32** | Attribute group order among other attribute groups | [optional] [default to null]
**Attributes** | **[]string** | Attribute codes that compose the attribute group | [optional] [default to null]
**Labels** | [***Apirestv1attributegroupsLabels**](apirestv1attributegroups_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


