# Body25

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Channel code | [default to null]
**Locales** | **[]string** | Codes of activated locales for the channel | [default to null]
**Currencies** | **[]string** | Codes of activated currencies for the channel | [default to null]
**CategoryTree** | **string** | Code of the category tree linked to the channel | [default to null]
**ConversionUnits** | [***Apirestv1channelsConversionUnits**](apirestv1channels_conversion_units.md) |  | [optional] [default to null]
**Labels** | [***Apirestv1channelsLabels**](apirestv1channels_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


