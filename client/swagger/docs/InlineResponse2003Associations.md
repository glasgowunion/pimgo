# InlineResponse2003Associations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssociationTypeCode** | [***InlineResponse2003AssociationsAssociationTypeCode**](inline_response_200_3_associations_associationTypeCode.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


