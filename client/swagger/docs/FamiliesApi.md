# \FamiliesApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetFamilies**](FamiliesApi.md#GetFamilies) | **Get** /api/rest/v1/families | Get list of families
[**GetFamiliesCode**](FamiliesApi.md#GetFamiliesCode) | **Get** /api/rest/v1/families/{code} | Get a family
[**GetFamiliesFamilyCodeVariants**](FamiliesApi.md#GetFamiliesFamilyCodeVariants) | **Get** /api/rest/v1/families/{family_code}/variants | Get list of family variants
[**GetFamiliesFamilyCodeVariantsCode**](FamiliesApi.md#GetFamiliesFamilyCodeVariantsCode) | **Get** /api/rest/v1/families/{family_code}/variants/{code} | Get a family variant
[**PatchFamilies**](FamiliesApi.md#PatchFamilies) | **Patch** /api/rest/v1/families | Update/create several families
[**PatchFamiliesCode**](FamiliesApi.md#PatchFamiliesCode) | **Patch** /api/rest/v1/families/{code} | Update/create a family
[**PatchFamiliesFamilyCodeVariants**](FamiliesApi.md#PatchFamiliesFamilyCodeVariants) | **Patch** /api/rest/v1/families/{family_code}/variants | Update/create several family variants
[**PatchFamiliesFamilyCodeVariantsCode**](FamiliesApi.md#PatchFamiliesFamilyCodeVariantsCode) | **Patch** /api/rest/v1/families/{family_code}/variants/{code} | Update/create a family variant
[**PostFamilies**](FamiliesApi.md#PostFamilies) | **Post** /api/rest/v1/families | Create a new family
[**PostFamiliesFamilyCodeVariants**](FamiliesApi.md#PostFamiliesFamilyCodeVariants) | **Post** /api/rest/v1/families/{family_code}/variants | Create a new family variant


# **GetFamilies**
> interface{} GetFamilies(optional)
Get list of families

This endpoint allows you to get a list of families. Families are paginated and sorted by code.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFamiliesCode**
> InlineResponse2005 GetFamiliesCode(code)
Get a family

This endpoint allows you to get the information about a given family.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse2005**](inline_response_200_5.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFamiliesFamilyCodeVariants**
> interface{} GetFamiliesFamilyCodeVariants(familyCode, optional)
Get list of family variants

This endpoint allows you to get a list of family variants. Family variants are paginated and sorted by code.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **familyCode** | **string**| Code of the family | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **familyCode** | **string**| Code of the family | 
 **page** | **int32**| Number of the page to retrieve when using the &#x60;page&#x60; pagination method type. &lt;strong&gt;Should never be set manually&lt;/strong&gt;, see &lt;a href&#x3D;\&quot;/documentation/pagination.html#pagination\&quot;&gt;Pagination&lt;/a&gt; section | [default to 1]
 **limit** | **int32**| Number of results by page, see &lt;a href&#x3D;\&quot;/documentation/pagination.html\&quot;&gt;Pagination&lt;/a&gt; section | [default to 10]
 **withCount** | **bool**| Return the count of products in the response. Be carefull with that, on a big catalog, it can decrease performance in a significative way | [default to false]

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetFamiliesFamilyCodeVariantsCode**
> InlineResponse2006 GetFamiliesFamilyCodeVariantsCode(familyCode, code)
Get a family variant

This endpoint allows you to get the information about a given family variant.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **familyCode** | **string**| Code of the family | 
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse2006**](inline_response_200_6.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchFamilies**
> InlineResponse200 PatchFamilies(optional)
Update/create several families

This endpoint allows you to update and/or create several families at once.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body10**](Body10.md)|  | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchFamiliesCode**
> PatchFamiliesCode(code, body)
Update/create a family

This endpoint allows you to update a given family. Know more about <a href=\"/documentation/update.html#update-behavior\">Update behavior</a>. Note that if no family exists for the given code, it creates it.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 
  **body** | [**Body11**](Body11.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchFamiliesFamilyCodeVariants**
> InlineResponse200 PatchFamiliesFamilyCodeVariants(familyCode, optional)
Update/create several family variants

This endpoint allows you to update and/or create several family variants at once, for a given family.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **familyCode** | **string**| Code of the family | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **familyCode** | **string**| Code of the family | 
 **body** | [**Body13**](Body13.md)|  | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchFamiliesFamilyCodeVariantsCode**
> PatchFamiliesFamilyCodeVariantsCode(familyCode, code, body)
Update/create a family variant

This endpoint allows you to update a given family variant. Know more about <a href=\"/documentation/update.html#update-behavior\">Update behavior</a>. Note that if no family variant exists for the given code, it creates it.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **familyCode** | **string**| Code of the family | 
  **code** | **string**| Code of the resource | 
  **body** | [**Body14**](Body14.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostFamilies**
> PostFamilies(optional)
Create a new family

This endpoint allows you to create a new family.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Body9**](Body9.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostFamiliesFamilyCodeVariants**
> PostFamiliesFamilyCodeVariants(familyCode, optional)
Create a new family variant

This endpoint allows you to create a family variant.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **familyCode** | **string**| Code of the family | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **familyCode** | **string**| Code of the family | 
 **body** | [**Body12**](Body12.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

