# \MeasureFamiliesApi

All URIs are relative to *http://demo.akeneo.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**MeasureFamiliesGet**](MeasureFamiliesApi.md#MeasureFamiliesGet) | **Get** /api/rest/v1/measure-families/{code} | Get a measure family
[**MeasureFamiliesGetList**](MeasureFamiliesApi.md#MeasureFamiliesGetList) | **Get** /api/rest/v1/measure-families | Get list of measure familiy


# **MeasureFamiliesGet**
> InlineResponse20013 MeasureFamiliesGet(code)
Get a measure family

This endpoint allows you to get the information about a given measure family.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **code** | **string**| Code of the resource | 

### Return type

[**InlineResponse20013**](inline_response_200_13.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MeasureFamiliesGetList**
> interface{} MeasureFamiliesGetList()
Get list of measure familiy

This endpoint allows you to get a list of measure families. Measure families are paginated and sorted by code.

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**interface{}**](interface{}.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

