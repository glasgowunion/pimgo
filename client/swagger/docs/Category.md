# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Category code | [default to null]
**Parent** | **string** | Category code of the parent&#39;s category | [optional] [default to null]
**Labels** | [***Apirestv1categoriesLabels**](apirestv1categories_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


