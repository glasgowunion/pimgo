# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Identifier** | **string** | Published product identifier, i.e. the value of the only &#x60;pim_catalog_identifier&#x60; attribute | [default to null]
**Enabled** | **bool** | Whether the published product is enable | [optional] [default to null]
**Family** | **string** | Family code from which the published product inherits its attributes and attributes requirements | [optional] [default to null]
**Categories** | **[]string** | Codes of the categories in which the published product is classified | [optional] [default to null]
**Groups** | **[]string** | Codes of the groups to which the published product belong | [optional] [default to null]
**Values** | [***InlineResponse2003Values**](inline_response_200_3_values.md) |  | [optional] [default to null]
**Associations** | [***InlineResponse2003Associations**](inline_response_200_3_associations.md) |  | [optional] [default to null]
**Created** | **string** | Date of creation | [optional] [default to null]
**Updated** | **string** | Date of the last update | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


