# Body20

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** | Code of option | [default to null]
**Attribute** | **string** | Code of attribute related to the attribute option | [optional] [default to null]
**SortOrder** | **int32** | Order of attribute option | [optional] [default to null]
**Labels** | [***Apirestv1attributesattributeCodeoptionsLabels**](apirestv1attributesattribute_codeoptions_labels.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


