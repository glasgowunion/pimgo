/* 
 * Akeneo PIM API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 1.0.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type InlineResponse201Errors struct {

	// Channel for which the variation file generation failed
	Channel string `json:"channel,omitempty"`

	// Locale for which the variation file generation failed
	Locale string `json:"locale,omitempty"`

	// Message explaining why the variation file generation failed
	Message string `json:"message,omitempty"`
}
