/* 
 * Akeneo PIM API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 1.0.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type Body26 struct {

	// Channel code
	Code string `json:"code"`

	// Codes of activated locales for the channel
	Locales []string `json:"locales"`

	// Codes of activated currencies for the channel
	Currencies []string `json:"currencies"`

	// Code of the category tree linked to the channel
	CategoryTree string `json:"category_tree"`

	ConversionUnits *Apirestv1channelsConversionUnits `json:"conversion_units,omitempty"`

	Labels *Apirestv1channelsLabels `json:"labels,omitempty"`
}
